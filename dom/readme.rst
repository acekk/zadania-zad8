
Praca domowa #8
===============

* Dokończyć zadanie laboratoryjne.
* Dodać możliwość samodzielnej rejestracji użytkowników.
  Użytkownik powinien być aktywowany dopiero po potwierdzeniu podanego adresu e-mail
  (np. przez kliknięcie linka aktywacyjnego przesłanego pod wskazany adres).
* Umożliwić użytkownikowi edycję swojego wpisu, gdy nie minęło więcej niż 10 min od jego utworzenia
  lub ostatniej edycji.
* Stworzyć grupę moderatorów, która będzie miała uprawnienia do edycji wpisów innych osób niezależnie
  od czasu utworzenia wpisu.
* Każdorazowo po wyedytowaniu wpisu (przez użytkownika lub moderatora) powinna pojawić się dodatkowa
  (obok czasu utworzenia) informacja o czasie ostatniej edycji i ostatnim edytorze.