from django.db import models

class Tag(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

class Entry(models.Model):
	title = models.CharField(max_length=100)
	author = models.CharField(max_length=25)
	content = models.TextField(max_length=200)
	add_date = models.DateTimeField()

	tags = models.ManyToManyField(Tag, blank=True, null=True)

	editor = models.CharField(max_length=100)
	edit_date = models.DateTimeField()

	def __unicode__(self):
		return self.title

