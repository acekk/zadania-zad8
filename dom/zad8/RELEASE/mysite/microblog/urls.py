from django.conf.urls import patterns, url
from django.views.generic import ListView, UpdateView

from models import Entry
from forms import AddEntryForm, EditEntryForm
from views import add_entry, edit_entry, list_entry, list_tag_entry, list_user_entry

import views

urlpatterns = patterns('',
	url(r'^$', list_entry, name='list_entry'),
	url(r'^add/$', add_entry, name='add_entry'),
	url(r'^edit/(?P<pk>\d+)/$', edit_entry, name='edit_entry'),
	url(r'^login/$', views.login_view, name='login'),
	url(r'^logout/$', views.logout_view, name='logout'),
	url(r'^register/$', views.register, name='register'),
	url(r'^tag/(?P<tag>.*)/$', list_tag_entry, name='list_tag_entry'),
	url(r'^user/(?P<user>.*)/$', list_user_entry, name='list_user_entry'),
)
