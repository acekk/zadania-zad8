from django import forms
from models import Entry, Tag

class EditEntryForm(forms.Form):
	title = forms.CharField(label='Title', min_length=0, max_length=100)
	content = forms.CharField(label='Content', min_length=10, max_length=200)
	tags = forms.ModelMultipleChoiceField(label='Tags', queryset=Tag.objects.all().order_by('name'), required=False)

class AddEntryForm(forms.Form):
	author = forms.CharField(label='Author', widget=forms.TextInput(attrs={'class' : 'disabled', 'readonly' : 'readonly'}))
	title = forms.CharField(label='Title', min_length=0, max_length=100)
	content = forms.CharField(label='Content', min_length=10, max_length=200)
	tags = forms.ModelMultipleChoiceField(label='Tags', queryset=Tag.objects.all().order_by('name'), required=False)

class LoginForm(forms.Form):
	username = forms.CharField(label='Login', min_length=1)
	password = forms.CharField(label='Password', min_length=1, widget=forms.PasswordInput())

class RegisterForm(forms.Form):
	username = forms.CharField(label='Login', min_length=1)
	password1 = forms.CharField(label='Password', min_length=1, widget=forms.PasswordInput())
	password2 = forms.CharField(label='Password', min_length=1, widget=forms.PasswordInput())
