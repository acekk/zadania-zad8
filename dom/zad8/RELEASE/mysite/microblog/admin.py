from django.contrib import admin
from microblog.models import Entry, Tag

class ExtendedEntry(admin.ModelAdmin):
	list_display = ['title', 'add_date', 'edit_date']
	list_filter = ['add_date']
	ordering = ['-add_date']
	search_fields = ['tags__name']

admin.site.register(Entry, ExtendedEntry)
admin.site.register(Tag)
