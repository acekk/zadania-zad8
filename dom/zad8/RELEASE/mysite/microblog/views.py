from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from django.contrib.auth.models import User
from django.views.generic import UpdateView
from django.contrib.auth.forms import UserCreationForm

from forms import AddEntryForm, EditEntryForm, LoginForm, RegisterForm
from models import Entry, Tag

def entryFormValidation(form):
	title = form.cleaned_data['title']
	content = form.cleaned_data['content']
	if len(title) > 100 or len(content) < 10 or len(content) > 200:
		return False
	return True

def loginFormValidation(form):
	username = form.cleaned_data['username']
	password = form.cleaned_data['password']
	if len(username) == 0 or len(password) == 0:
		return False
	return True

def registerFormValidation(form):
	username = form.cleaned_data['username']
	password1 = form.cleaned_data['password1']
	password2 = form.cleaned_data['password2']
	if len(username) == 0 or len(password1) == 0 or len(password2) == 0:
		return False
	if password1 != password2:
		return False
	return True

def add_entry(request):
	resp = {}
	resp.update(csrf(request))

	if not request.user.is_authenticated():
		return render_to_response('microblog/first_login.html', resp)

	resp['username'] = request.user.username
	resp['form'] = AddEntryForm(initial = { 'author' : resp['username'] })

	if request.method != 'POST':
		return render_to_response('microblog/add_entry.html', resp)

	form = AddEntryForm(request.POST)

	if not (form.is_valid() and entryFormValidation(form)):
		resp['message'] = 'Correct form fields!'
		return render_to_response('microblog/add_entry.html', resp)

	entry = Entry(
		author=form.cleaned_data['author'],
		title=form.cleaned_data['title'],
		content=form.cleaned_data['content'],
		add_date = timezone.now(),
		editor=form.cleaned_data['author'],
		edit_date = timezone.now()
	)
	entry.save()
	for tag in form.cleaned_data['tags']:
		entry.tags.add(tag)

	resp['entries'] = Entry.objects.order_by('-add_date')
	resp['message'] = 'Correctly added!'
	return render_to_response('microblog/add_entry.html', resp)

def common_entry(request, entries):
	username = None
	for entry in entries:
		entry.modifiable = False

	if request.user.is_authenticated():
		username = request.user.username
		isEditor = (request.user.groups.filter(name='editors').count() > 0)
		for entry in entries:
			isAuthor = (entry.author == username)
			timeCond = ((timezone.now() - entry.edit_date).seconds < 600)
			entry.modifiable = (isAuthor and timeCond) or isEditor

	return username, entries

def edit_entry(request, pk):
	resp = {}
	resp.update(csrf(request))

	if not request.user.is_authenticated():
		resp['message'] = 'You have no permission to edit this entry!'
		return render_to_response('microblog/list_entry.html', resp)

	resp['username'] = request.user.username

	entry = Entry.objects.get(pk=pk)
	resp['form'] = EditEntryForm(initial = {
		'title' : entry.title,
		'content' : entry.content,
		'tags' : entry.tags.all()
	})

	isEditor = (request.user.groups.filter(name='editors').count() > 0)
	isAuthor = (entry.author == resp['username'])
	timeCond = ((timezone.now() - entry.edit_date).seconds < 600)
	if not ((isAuthor and timeCond) or isEditor):
		resp['message'] = 'You have no permission to edit this entry!'
		return render_to_response('microblog/list_entry.html', resp)

	if request.method != 'POST':
		return render_to_response('microblog/edit_entry.html', resp)

	form = EditEntryForm(request.POST)
	if not (form.is_valid() and entryFormValidation(form)):
		resp['message'] = 'Correct form fields!'
		return render_to_response('microblog/edit_entry.html', resp)

	entry.title=form.cleaned_data['title']
	entry.content=form.cleaned_data['content']
	entry.editor=resp['username']
	entry.edit_date = timezone.now()
	entry.save()
	
	entry.tags.clear()
	for tag in form.cleaned_data['tags']:
		entry.tags.add(tag)

	resp['entries'] = Entry.objects.order_by('-add_date')
	resp['message'] = 'Correctly added!'
	resp['form'] = EditEntryForm(initial = {
		'title' : form.cleaned_data['title'],
		'content' : form.cleaned_data['content'],
		'tags' : entry.tags.all()
	})
	return render_to_response('microblog/edit_entry.html', resp)

def list_entry(request):
	username, entries = common_entry(request, Entry.objects.order_by('-add_date'))

	return render_to_response('microblog/list_entry.html', locals())

def list_tag_entry(request, tag):
	username, entries = common_entry(request, Entry.objects.filter(tags=Tag.objects.get(name=tag).id).order_by('-add_date'))

	return render_to_response('microblog/list_entry.html', locals())

def list_user_entry(request, user):
	username, entries = common_entry(request, Entry.objects.filter(author=user).order_by('-add_date'))

	return render_to_response('microblog/list_entry.html', locals())

def login_view(request):
	resp = {}
	resp.update(csrf(request))
	resp['form'] = LoginForm()

	if request.user.is_authenticated():
		resp['username'] = request.user.username
		return render_to_response('microblog/login.html', resp)

	if request.method != 'POST':
		return render_to_response('microblog/login.html', resp)

	form = LoginForm(request.POST)
	if not (form.is_valid() and loginFormValidation(form)):
		resp['message'] = 'Correct form fields!'
		return render_to_response('microblog/login.html', resp)

	username = form.cleaned_data['username']
	password = form.cleaned_data['password']
	user = authenticate(username=username, password=password)
	if user is not None:
		login(request, user)
		resp['username'] = username
	else:
		resp['message'] = 'Incorrect login or password!'

	return render_to_response('microblog/login.html', resp)

def logout_view(request):
	resp = {}
	resp.update(csrf(request))

	logout(request)
	resp['message'] = 'You are correctly logout!'
	resp['form'] = LoginForm()

	return render_to_response('microblog/logout.html', resp)

def register(request):
	resp = {}
	resp.update(csrf(request))

	resp['form'] = RegisterForm()

	if request.user.is_authenticated():
		resp['username'] = request.user.username
		return render_to_response('microblog/register.html', resp)

	if request.method != 'POST':
		return render_to_response('microblog/register.html', resp)

	form = RegisterForm(request.POST)
	if not (form.is_valid() and registerFormValidation(form)):
		resp['message'] = 'Correct form fields!'
		print "KOTKOTKOT"
		return render_to_response('microblog/register.html', resp)

	username = form.cleaned_data['username']
	password = form.cleaned_data['password1']

	if User.objects.filter(username=username).count() > 0:
		resp['message'] = 'This login already exist!'
		return render_to_response('microblog/register.html', resp)

	User.objects.create_user(username, '', password)
	resp['message'] = 'Your account was created!'
	return render_to_response('microblog/register.html', resp)
